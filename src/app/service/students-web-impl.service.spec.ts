import { TestBed } from '@angular/core/testing';

import { StudentsWebImplService } from './students-web-impl.service';

describe('StudentsWebImplService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StudentsWebImplService = TestBed.get(StudentsWebImplService);
    expect(service).toBeTruthy();
  });
});
