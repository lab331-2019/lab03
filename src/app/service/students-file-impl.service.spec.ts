import { TestBed } from '@angular/core/testing';

import { StudentsFileImplService } from './students-file-impl.service';

describe('StudentsFileImplService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StudentsFileImplService = TestBed.get(StudentsFileImplService);
    expect(service).toBeTruthy();
  });
});
