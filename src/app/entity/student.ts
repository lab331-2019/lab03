export class Student {
    id: number;
    studentId: string;
    name: string;
    surname: string;
    gpa: number;
	image: string;
	penAmount: number;
}
